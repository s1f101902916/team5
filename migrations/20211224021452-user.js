'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('Users', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      name: {
        type: Sequelize.STRING,
        primaryKey: true
      },
      pass: {
        type: Sequelize.STRING
      },
      profile: {
        type: Sequelize.TEXT
      },
      icon: {
        type: Sequelize.STRING,
        defaultValue: "personalaikon.png"
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      hashedpass: {
        type: Sequelize.JSON
      },
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('Users');
  }
};