var express = require('express');
var router = express.Router();

var app = express();

//mysqlとの接続
const mysql = require('mysql2');
const path = require('path');
const bodyParser = require('body-parser');
const ejs = require('ejs');
const { runInNewContext } = require('vm');
const con = require('./const');

app.use(bodyParser.urlencoded({ extended: true }));
app.set('view engine', 'ejs');
con.connect();
const pool = require('../db/pool');

router.get('/:id',(req,res)=>{
	const sql = "SELECT * FROM posts WHERE post_id = ?";
	pool.getConnection(function(err, connection){
		connection.query(sql,[req.params.id],function (err, result, fields) {
			if (err) throw err;
			res.render('u_update',{user : result});
			connection.release();
			});
		});
});

module.exports = router;
