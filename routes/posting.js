var express = require('express');
var router = express.Router();
const exphbs = require('express-handlebars');
require('date-utils');

var app = express();

//mysqlとの接続
const mysql = require('mysql2');
const path = require('path');
const bodyParser = require('body-parser');
const ejs = require('ejs');
const { runInNewContext } = require('vm');
const con = require('./const');
const pool = require('../db/pool');

app.use(bodyParser.urlencoded({ extended: true }));
app.engine('ejs', exphbs({ extname: '.ejs'}));
app.set('view engine', 'ejs');

//セッション（ログインしていないと入れないように）
function check(req,res){
  if(req.session.login == null ){
      req.session.back = 'posting';
      res.redirect('/users/login');
      return true;
  }else{
      return false;
  }
}

/* GET posting page. */

router.get('/', function(req, res, next) {
    if(check(req,res)){ return };
    const sql_p = 'select * from posts';
    pool.getConnection(function(err, connection){
      connection.query(sql_p, function (err, result, fields) {
          if (err) throw err;
          //表示するときはターミナルを見ればわかる通り配列で渡されていることに注意
          res.render('posting', { title: result });
          connection.release();
      });
    });
});

router.get('/', (req, res) =>
  res.sendFile(path.join(__dirname, 'views/posting.ejs')));

router.post('/', (req, res) => {
  const now = new Date();
  let sampleFile;

  if(!req.files || Object.keys(req.files).length === 0){
    return res.status(400).send('No files were uploaded');
  }

  sampleFile = req.files.sampleFile;

  var base64str = sampleFile.data.toString('base64');
  req.body.uploaded_file = base64str;

  req.body.name = req.session.login.name;
  pool.getConnection(function(err, connection){
    connection.query('INSERT INTO posts SET ?', [req.body], (err, rows) => {
      if (!err) {
        //res.send('登録が完了しました!!!!!^_^"');
        return res.redirect('/mypage/home/0/0');

      } else {
        console.log(err);
        return res.status(500).send(err);
      }

    });
    connection.release();
  });
});

module.exports = router;
