var express = require('express');
var router = express.Router();

var app = express();


const mysql = require('mysql2');
const path = require('path');
const bodyParser = require('body-parser');
const ejs = require('ejs');
const { runInNewContext } = require('vm');
const con = require('./const');


app.use(bodyParser.urlencoded({ extended: true }));
app.set('view engine', 'ejs');
con.connect();

/* GET posting page. */
router.get('/', function(req, res, next) {
    const sql_p = 'SELECT DISTINCT category1 FROM posts';

    con.query(sql_p, function (err, result, fields) {

        if (err) throw err;

        //表示するときはターミナルを見ればわかる通り配列で渡されていることに注意
        res.render('category/index', { users: result});

    });
});

router.get('/:tag',(req, res, next) => {
    con.query('select * from posts where category1 = ? or category2 = ? order by views', [req.params.tag, req.params.tag]  , function (err, result, fields) {

    res.render('category/tags' , { users: result, cate: req.params.tag});
    });
});

module.exports = router;
