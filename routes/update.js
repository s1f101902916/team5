var express = require('express');
var router = express.Router();
const exphbs = require('express-handlebars');
require('date-utils');

var app = express();

//mysqlとの接続
const mysql = require('mysql2');
const path = require('path');
const bodyParser = require('body-parser');
const ejs = require('ejs');
const { runInNewContext } = require('vm');
const con = require('./const');

app.use(bodyParser.urlencoded({ extended: true }));
app.engine('ejs', exphbs({ extname: '.ejs'}));
app.set('view engine', 'ejs');
con.connect();
const pool = require('../db/pool');

router.post('/:id', (req, res) => {
    const now = new Date();
    var time = now.toFormat('YYYYMMDDHH24MISS');
    let sampleFile;
    let uploadPath;

    if(!req.files || Object.keys(req.files).length === 0){
      req.params.uploaded_file = req.body.uploaded_file;
    } else {
      sampleFile = req.files.sampleFile;
      uploadPath = path.resolve(__dirname, '..') + '/img/' + time + sampleFile.name;
      sampleFile.mv(uploadPath, function(err) {
        if(err) return res.status(500).send(err);
      });
      var base64str = sampleFile.data.toString('base64');
      req.body.uploaded_file = base64str;
    }

    const sql1 = "UPDATE posts SET ? WHERE post_id = " + req.params.id;
    pool.getConnection(function(err, connection){
      connection.query(sql1, req.body, function (err, result, fields) {
          if (err) throw err;
          console.log(result);
          res.redirect('/mypage/home/0/0');
          connection.release();
      });
    });
});

module.exports = router;
