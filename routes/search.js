var express = require('express');
var router = express.Router();

var app = express();


//mysqlとの接続
const mysql = require('mysql2');
const path = require('path');
const bodyParser = require('body-parser');
const ejs = require('ejs');
const { runInNewContext } = require('vm');
const con = require('./const');

app.use(bodyParser.urlencoded({ extended: true }));
app.set('view engine', 'ejs');
con.connect();
const pool = require('../db/pool');

/* GET posting page. */
router.get('/', function(req, res, next){
  const sql_p='select * from posts';
  pool.getConnection(function(err, connection){
    connection.query(sql_p, function (err, result, fields) {
      if(err) throw err;
      var data = {
        title: 'search',
        find: '',
        content: '検索条件を入力してください',
        mydata: result
      };
      res.render('search', data);
      connection.release();
    });
  });
});

router.post('/', (req, res, next) =>{
  var find = req.body.find;

  //sqlインジェクション対策
  var check = find.match(/\"|\=|\>|\<|\,/);      
  if(check != null || find.indexOf('\'') != -1){
    var data = {
    title: '記号は使用しないで下さい',
    find: find,
    content: '検索条件を入力してください',
    mydata: null
    };
    res.render('search', data);
  }
   
  else{
    var separator = /\s+/;
    var array = find.split(separator);  
    var q = "select * from posts where ";
    
    if (array.length > 1) {
      q += "text like '%";    
      for (let i = 0; i < array.length; i++) { 
        if (i>0){q += "OR text like'%";}      
        q += array[i]+"%' OR title like'%" + array[i] + "%' " ;
      }
    }
    else{
      q += "title like '%" + find +"%' OR text like '%"+find+"%'"
    }  

    con.query(q, function (err, result, fields) {
      if(err) throw err;
      else{
        var data = {
        title: find + 'の検索結果',
        find: find,
        content: '検索条件を入力してください',
        mydata: result
        };
      }
      res.render('search', data);
    });
  }
});
module.exports = router;