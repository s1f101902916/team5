var express = require('express');
var router = express.Router();
require('date-utils');
const path = require('path');
var app = express();
app.use(express.static(__dirname));
const db = require('../models/index');
const { Op } = require("sequelize");

function check(req,res){
  if(req.session.login == null ){
      req.session.back = '/settings/profile';
      res.redirect('/users/login');
      return true;
  }else{
      return false;
  }
}

router.get('/profile', (req, res, next) => {
  if(check(req,res)){ return };
  db.User.findAll({ where: { name: req.session.login.name } }).then(users => {

    var data = {
      name: req.session.login.name,
      prf: users
    }
    res.render("settings/profile",data);

  });

});

router.post('/profile/add',(req, res, next) => {
  db.User.update(
    { profile: req.body.msg },
    { where: { name:  req.session.login.name} }
  ).then(() => {

    res.redirect("/settings/profile");

  });
});

router.post('/profile/icon',(req, res, next) => {
  var now = new Date();  //画像名前変更のため日付を出しておく
  var time = now.toFormat('YYYYMMDDHH24MISS')
  var sampleFile = req.files.sampleFile;
  var uploadPath = path.resolve(__dirname, '..') + '/public/icon/' + time + sampleFile.name;
  console.log(sampleFile);
  sampleFile.mv(uploadPath, function(err) {
    if(err) return res.status(500).send(err);
  });

  req.body.uploaded_file = time+sampleFile.name;
  req.body.name = req.session.login.name;

  db.User.update(
    { icon: req.body.uploaded_file },
    { where: { name:  req.session.login.name} }
  ).then(() => {

    res.redirect("/settings/profile");

  });
});

module.exports = router;