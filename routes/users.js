var express = require('express');
var router = express.Router();
const db = require('../models/index');

/* GET users listing. */
router.get('/', function(req, res, next) {
  db.User.findAll().then(usrs => {
    var data = {
      title: 'Users/Index',
      content: usrs
    }
    res.render('users/index', data);
  });
});

router.get('/login', (req, res, next) => {
  var data = {
    title:'エコシルにログイン',
    content:'ユーザー名とパスワードを入力してください。'

  }
  res.render('users/login', data);
});

router.post('/login', (req, res, next) => {
  db.User.findOne({
    where:{
      name:req.body.name,
      pass:req.body.pass,
    }
  }).then (usr=>{
    if (usr != null ){
      req.session.login = usr;
      let back = req.session.back;
      if (back == 'mypage/home/0/0'){
        res.redirect('/mypage/home/0/0');
      }
      if (back == 'posting'){
        res.redirect('/posting');
      }
      if(back == null){
        res.redirect('/mypage/home/0/0')
      }else{
        res.redirect(back);
      }




    }else{
      var data = {
        title:'エコシルにログイン',
        content:'名前かパスワードに問題があります。再度入力下さい。'
      }
      res.render('users/login', data);
    }
  })
});

router.get('/add' ,(req, res, next)=>{
  var data = {
    title: 'エコシルにようこそ！',
    content: 'ユーザー名とパスワードを登録してください'
  }
  res.render('users/add', data);
});

router.post('/add',(req, res, next)=>{
  db.User.findOne({
    where:{
      name:req.body.name,
    }
  }).then (tem=>{
    if(tem == null){
      if(req.body.pass.length>5){
        db.sequelize.sync()
        .then(() => db.User.create({
          name: req.body.name,
          pass: req.body.pass
        }))
        .then(usr => {
          res.redirect('/users/login');
        });
      }else{
        var data = {
          title: 'エコシルにようこそ！',
          content: 'パスワードの文字数が足りません'
        }
        res.render('users/add', data);
      }}
    else{
      var data = {
        title: 'エコシルにようこそ！',
        content: 'このユーザ名は既に使われています'
      }
      res.render('users/add', data);
    }
});
});



router.get('/logout', (req, res) => {
  req.session.destroy((err) => {
    res.redirect('/');
  });
});

router.get('/delete',(req, res, next) => {
  db.User.destroy({
    where: {
      name:  req.session.login.name
    }
  }).then(() => {

    res.redirect('/');

  });
});

module.exports = router;