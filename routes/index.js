var express = require('express');
var router = express.Router();

var app = express();

//mysqlとの接続
const mysql = require('mysql2');
const path = require('path');
const bodyParser = require('body-parser');
const ejs = require('ejs');
const { runInNewContext } = require('vm');
const con = require('./const');

app.use(bodyParser.urlencoded({ extended: true }));
app.set('view engine', 'ejs');
con.connect();
const pool = require('../db/pool');

/* GET posting page. */
router.get('/', function(req, res, next) {
  pool.getConnection(function(err, connection){
      const sql_p = 'select * from posts where types = "academic" order by post_id desc limit 5';
      const sql_p2 = 'select * from posts where types = "general" order by post_id desc limit 5';
      const sql_p3 = 'select * from posts order by views desc limit 5';
      const sql_p4 = 'select name from hc4f92y3uzc7vp7p.posts group by name order by sum(likes) desc limit 5';
      connection.query(sql_p, function (err, result, fields) {
        connection.query(sql_p2, function (err, result2, fields) {
          connection.query(sql_p3, function (err, result3, fields) {
            connection.query(sql_p4, function (err, result4, fields) {
            if (err) throw err;

            //表示するときはターミナルを見ればわかる通り配列で渡されていることに注意
            res.render('index', { newrank_academic: result , newrank_general: result2, viewrank: result3, user_rank: result4});
            connection.release();
            });
          });
        });
      });
    });
});
/*
router.get('/', (req, res) =>
  res.sendFile(path.join(__dirname, 'views/edit.ejs')));
*/

module.exports = router;
