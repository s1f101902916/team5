var express = require('express');
var router = express.Router();

var app = express();
app.use(express.static(__dirname));
const db = require('../models/index');
const { Op } = require("sequelize");

//mysqlとの接続
const mysql = require('mysql2');
const path = require('path');
const bodyParser = require('body-parser');
const ejs = require('ejs');
const { runInNewContext } = require('vm');
const con = require('./const');

app.use(bodyParser.urlencoded({ extended: true }));
app.set('view engine', 'ejs');
con.connect();
const pool = require('../db/pool');

//セッション（ログインしていないと入れないように）
function check(req,res){
  if(req.session.login == null ){
      req.session.back = 'mypage/home/0/0';
      res.redirect('/users/login');
      return true;
  }else{
      return false;
  }
}
function check2(req,res,id){
  if(req.session.login == null ){
      req.session.back = '/mypage/article/'+  id  + '/0';
      res.redirect('/users/login');
      return true;
  }else{
      return false;
  }
}





router.get('/home/0/0', function(req, res, next) {
  if(check(req,res)){ return };
  pool.getConnection(function(err, connection){
    connection.query('select * from posts where name = ?', [req.session.login.name], function (err, result, fields) {
        if (err) throw err;
        connection.query('select likes from posts where name = ?', [req.session.login.name], function (err, result1, fields) {
          db.Article.sum('likes', { where: { name: [req.session.login.name] } }).then(cmlikes => {
            if(cmlikes>=0){
              res.render('mypage/home', { users: result, name: req.session.login.name, sumlikes: result1, cmlikes: cmlikes});
            }else{
              res.render('mypage/home', { users: result, name: req.session.login.name, sumlikes: result1, cmlikes: 0});
            }
            connection.release();


          });

        });

    });
  });
});

router.get('/home/:user/1',(req, res, next) => {
  pool.getConnection(function(err, connection){
    connection.query('select * from posts where name = ?', [req.params.user], function (err, result, fields) {
        if (err) throw err;
        res.render('mypage/others', { users: result, name: req.params.user});
        connection.release();
    });
  });
})


router.post('/article/:id/0',(req, res, next) => {
  if(check2(req,res,req.params.id)){ return };
  db.good.findOne({
    where:{
      name:  req.session.login.name,
      artId: req.params.id,
      comeId: -1
    }
  }).then (usr=>{
    if(usr!=null){
      usr.destroy();
      pool.getConnection(function(err, connection){
        connection.query('select likes from posts where post_id = ?', [req.params.id], function (err, result, fields) {

          const sql1 = "UPDATE posts SET likes = ? WHERE post_id = " + req.params.id;
          connection.query(sql1, result[0].likes-1, function (err, result2, fields) {
              if (err) throw err;

              res.redirect('/mypage/article/'+  req.params.id  + '/0');
              connection.release();
          });

        });
    });

    }else{

      db.sequelize.sync()
      .then(() => db.good.create({
          userId: req.session.login.id,
          name:  req.session.login.name,
          artId: req.params.id,
          comeId: -1
      })
      .then(art=>{
        pool.getConnection(function(err, connection){
            connection.query('select likes from posts where post_id = ?', [req.params.id], function (err, result, fields) {

              const sql1 = "UPDATE posts SET likes = ? WHERE post_id = " + req.params.id;
              connection.query(sql1, result[0].likes+1, function (err, result2, fields) {
                  if (err) throw err;

                  res.redirect('/mypage/article/'+  req.params.id  + '/0')
                  connection.release();
              });

            });
        })
        .catch((err)=>{
            res.redirect('/mypage/article/'+  req.params.artid  + '/0')
            connection.release();
          });
        }))


  }

});
});

router.post('/article/add/:artid',(req, res, next) => {
  if(check(req,res)){ return };
  db.User.findAll({ where: { name: req.session.login.name } }).then(prf => {
    db.sequelize.sync()
    .then(() => db.Article.create({
      userId: req.session.login.id,
      name: req.session.login.name,
      message: req.body.msg,
      artId: req.params.artid,
      icon: prf[0].icon
    })
    .then(art=>{
      console.log("OK!!!");
      res.redirect('/mypage/article/'+  req.params.artid  + '/0')
    })
    .catch((err)=>{
      console.log(err);
      res.redirect('/mypage/article/'+  req.params.artid  + '/0')
    }))
  });
});

router.get('/article/:id/0',(req, res, next) => {
  pool.getConnection(function(err, connection){
    connection.query('select views from posts where post_id = ?', [req.params.id], function (err, result, fields) {
          const sql1 = "UPDATE posts SET views = ? WHERE post_id = " + req.params.id;
          connection.query(sql1, result[0].views+1, function (err, result2, fields) {
              if (err) throw err;
              connection.release();

          });
    });
  });
  if(req.session.login == null ){
    //コメント取り出し
    db.Article.findAll({ where: { artId: [req.params.id] } }).then(art => {
      pool.getConnection(function(err, connection){
        connection.query('select * from posts where post_id = ?', [req.params.id], function (err, result, fields) {
          db.User.findAll({ where: { name: result[0].name } }).then(prf => {
          if (err) throw err;
          var data = {
            content: art,
            id : result,
            Id : req.params.id,
            icon : "fas fa-thumbs-up",
            comeid: 0,
            prf: prf
          }
          res.render('mypage/article', data);
          connection.release();
        });
      });
    });
    });
  }else{
      //コメント取り出し
      db.Article.findAll({ where: { artId: [req.params.id] } }).then(art => {
        db.good.findOne({
          where:{
            name:  req.session.login.name,
            artId: req.params.id,
            comeId: -1
          }
        //usr=ユーザーが記事にいいねしているのかの情報
        }).then (usr=>{
          db.good.findAll({ where: { name:  req.session.login.name, artId: req.params.id, comeId:{ [Op.gt]: 0} } }).then(come => {

            if(come!=null){
                if(usr!=null){
                  pool.getConnection(function(err, connection){
                      connection.query('select * from posts where post_id = ?', [req.params.id], function (err, result, fields) {
                        db.User.findAll({ where: { name: result[0].name } }).then(prf => {
                        if (err) throw err;
                        var data = {
                          content: art,
                          id : result,
                          Id : req.params.id,
                          icon : "fas fa-check-circle",
                          comeid: come,
                          prf: prf
                        }
                        res.render('mypage/article', data);
                        connection.release();
                        });
                      });
                  });
              }else{
                pool.getConnection(function(err, connection){
                      connection.query('select * from posts where post_id = ?', [req.params.id], function (err, result, fields) {
                        db.User.findAll({ where: { name: result[0].name } }).then(prf => {
                        if (err) throw err;
                        var data = {
                          content: art,
                          id : result,
                          Id : req.params.id,
                          icon : "fas fa-thumbs-up",
                          comeid: come,
                          prf: prf
                        }
                        res.render('mypage/article', data);
                        connection.release();
                        });
                      });
                });

              }
          }else{
            if(usr!=null){
              pool.getConnection(function(err, connection){
                connection.query('select * from posts where post_id = ?', [req.params.id], function (err, result, fields) {
                  db.User.findAll({ where: { name: result[0].name } }).then(prf =>{
                  if (err) throw err;
                  var data = {
                    content: art,
                    id : result,
                    Id : req.params.id,
                    icon : "fas fa-check-circle",
                    comeid: 0,
                    prf: prf
                  }
                  res.render('mypage/article', data);
                  connection.release();
                });
                });
              });
            }else{
              pool.getConnection(function(err, connection){
              connection.query('select * from posts where post_id = ?', [req.params.id], function (err, result, fields) {
                db.User.findAll({ where: { name: result[0].name } }).then(prf =>{
                if (err) throw err;
                var data = {
                  content: art,
                  id : result,
                  Id : req.params.id,
                  icon : "fas fa-thumbs-up",
                  comeid: 0,
                  prf: prf
                }
                res.render('mypage/article', data);
                connection.release();
              })
              });
            });
            }

            }

        });
        });
      });



  }

});

//コメントのいいね動作
router.post('/article/good/:id/:comeid',(req, res, next) => {
  if(check2(req,res,req.params.id)){ return };
  //usr=そのコメントにいいねしているのか
  db.good.findOne({
    where:{
      name:  req.session.login.name,
      comeId: req.params.comeid,
      artId: req.params.id
    }
  }).then (usr=>{
    if(usr!=null){
      usr.destroy();
      db.Article.findByPk(req.params.comeid).then(user => {

          db.Article.update(
            { likes: user.likes-=1},{where: { id: req.params.comeid }}
          ).then(() => {
            res.redirect('/mypage/article/'+  req.params.id  + '/0')
          })
          .catch((err)=>{
            res.redirect('/mypage/article/'+  req.params.id  + '/0')
          })
      });



    }else{

      db.sequelize.sync()
      .then(() => db.good.create({
          userId: req.session.login.id,
          name:  req.session.login.name,
          comeId: req.params.comeid,
          artId: req.params.id
      })
      .then(()=>{
          db.Article.findByPk(req.params.comeid).then(user => {

            db.Article.update(
              { likes: user.likes+=1},{where: { id: req.params.comeid }}
            ).then(() => {
              res.redirect('/mypage/article/'+  req.params.id  + '/0')
            })
            .catch((err)=>{
              res.redirect('/mypage/article/'+  req.params.id  + '/0')
            })
        });


      })
    .catch((err)=>{
        res.redirect('/mypage/article/'+  req.params.id  + '/0')
    }))


  }

  });


});

router.get('/logout', (req, res) => {
  req.session.destroy((err) => {
    res.redirect('/');
  });
});

module.exports = router;
