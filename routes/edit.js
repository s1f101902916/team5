var express = require('express');
var router = express.Router();

var app = express();


//mysqlとの接続
const mysql = require('mysql2');
const path = require('path');
const bodyParser = require('body-parser');
const ejs = require('ejs');
const { runInNewContext } = require('vm');
const con = require('./const');

app.use(bodyParser.urlencoded({ extended: true }));
app.set('view engine', 'ejs');
con.connect();
const pool = require('../db/pool');
/* GET posting page. */
router.get('/', function(req, res, next) {
  connection.getConnection(function(err, connection){
    const sql_p = 'select * from posts';
      connection.query(sql_p, function (err, result, fields) {
          if (err) throw err;
          //表示するときはターミナルを見ればわかる通り配列で渡されていることに注意
          res.render('edit', { users: result });
          connection.release();
      });
  });
});
/*
router.get('/', (req, res) =>
  res.sendFile(path.join(__dirname, 'views/edit.ejs')));
*/

module.exports = router;
