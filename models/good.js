'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class good extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  good.init({
    userId: DataTypes.INTEGER,
    name: DataTypes.STRING,
    artId: DataTypes.INTEGER,
    comeId: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'good',
  });
  return good;
};