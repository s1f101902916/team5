var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var fileUpload = require('express-fileupload');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var mypageRouter = require('./routes/mypage');
var settingsRouter = require('./routes/settings');
var postingRouter = require('./routes/posting');
var editRouter = require('./routes/edit');
var updateRouter = require('./routes/update');
var updateidRouter = require('./routes/updateid');
var deleteRouter = require('./routes/delete');
var categoryRouter = require('./routes/category');
var searchRouter   = require('./routes/search');
var aboutRouter   = require('./routes/about');

const session = require('express-session');
var app = express();


var session_opt = {
  secret: 'keyboard cat',
  resave: false,
  saveUninitialized: false,
  cookie: { maxAge: 60 * 60 * 1000 }
};
app.use(session(session_opt));

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static('public'));
app.use(express.static(path.join(__dirname, 'img')));
app.use(fileUpload());

//ルーティング情報の設定
app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/mypage', mypageRouter);
app.use('/settings', settingsRouter);
app.use('/delete', deleteRouter);
app.use('/u_update', updateidRouter);
app.use('/posting', postingRouter);
app.use('/edit', editRouter);
app.use('/update', updateRouter);
app.use('/category', categoryRouter);
app.use('/search', searchRouter);
app.use('/about', aboutRouter);
// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
